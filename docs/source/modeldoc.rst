Models
==================

--------------------------------------
imakedata.model.raw
--------------------------------------

::

  from imakedata.model.raw import raw_model

  raw_model = [
      raw,
      academic_raw,
      ainu_raw,
      animal_raw,
      animal_name_raw,
      animal_prefix_raw,
      animal_suffix_raw,
      business_type_raw,
      car_raw,
      char_raw,
      ascii_letter_raw,
      ascii_lowercase_raw,
      ascii_uppercase_raw,
      digit_raw,
      hexdigit_raw,
      octdigit_raw,
      printable_raw,
      punctuation_raw,
      whitespace_raw,
      danish_raw,
      district_suffix_raw,
      english_raw,
      first_name_raw,
      geo_location_raw,
      job_title_raw,
      latin_raw,
      person_raw,
      first_name_raw,
      product_raw,
      product_name_raw,
      street_type_raw,
      surname_raw,
      training_course_raw,
      transport_raw,
      word_prefix_raw,
      word_suffix_raw,
  ]


--------------------------------------
imakedata.model.int
--------------------------------------

::

  from imakedata.model.int import int_model

  int_model = [
      zero_or_one,
      one_or_two,
      in_3,
      in_4,
      in_5,
      dice,
      in_7,
      in_8,
      in_9,
      in_10,
      in_11,
      dozen,
      in_20,
      in_24,
      in_50,
      in_100,
      in_1000,
      in_10000,
      town_populations,
      city_populations,
      country_populations,
      stars_in_galaxy,
  ]


--------------------------------------
imakedata.model.float
--------------------------------------

::

  from imakedata.model.float import float_model

  float_model = [
      quantum,
      fractional,
      grocery_price,
      consumer_price,
      gadget_price,
      car_price,
      house_price,
      national_gdp,
  ]


--------------------------------------
imakedata.model.time
--------------------------------------

::

  from imakedata.model.time import time_model

  time_model = [
      last_minute,
      last_hour,
      within_minutes,
      within_hours,
      opening_hours,
      last_24,
  ]

--------------------------------------
imakedata.model.date
--------------------------------------

::

  from imakedata.model.date import date_model

  date_model = [
      date_of_birth,
      date_of_birth_oap,
      date_of_birth_adult,
      date_of_birth_parent,
      date_of_birth_student,
      date_of_birth_teen,
      date_of_birth_child,
      date_of_birth_toddler,
      date_of_birth_baby,
      department,
      day_of_week,
      day_of_week_abrev,
      month_of_year,
      month_of_year_abrev,
      last_week,
      last_month,
      last_6_months,
      last_year,
      last_2_years,
      last_decade,
      last_century,
      within_days,
      within_weeks,
      within_months,
      within_12_months,
      within_years,
      sci_fi,
  ]


--------------------------------------
imakedata.model.location
--------------------------------------

::

  from imakedata.model.location import location_model, standard_address

  location_model = [
      posh_address1,
      address1,
      address2,
      locality,
      geo_location,
      post_code,
  ]

  standard_address = [
      address1,
      address2,
      locality,
      geo_location,
      post_code,
  ]


--------------------------------------
imakedata.model.blurb
--------------------------------------

::

  from imakedata.model.blurb import blurb_model

  blurb_model = [
      blurb,
      ainu_blurb,
      danish_blurb,
      english_blurb,
      latin_blurb,
      ainu_title,
      danish_title,
      english_title,
      latin_title,
      latin_motto,
  ]


--------------------------------------
imakedata.model.business
--------------------------------------

::

  from imakedata.model.business import business_model

  business_model = [
      employee_number,
      company_name,
      company_address1,
      company_address2,
      company_locality,
      company_geo_location,
      company_post_code,
      department,
      job_title,
      company_slug,
      company_domain,
      company_email,
      info_email,
      contact_email,
      sales_email,
      web_email,
      company_web,
      company_phone,
      company_fax,
  ]


--------------------------------------
imakedata.model.marketing
--------------------------------------

::

  from imakedata.model.marketing import marketing_model

  marketing_model = [
      postage_cost,
      product_weight,
      news_title,
      product_title,
      product_name,
      tweet,
      description,
      short_blurb,
      blurb,
      long_blurb,
      essay,
      foreign_chars,
  ]

  product_model = [
      product_name,
      short_blurb,
      tweet,
      product_weight,
      postage_cost,
  ]

--------------------------------------
imakedata.model.personal
--------------------------------------

::

  from imakedata.model.personal import personal_model

  personal_model = [
      title,
      first_name,
      last_name,
      gender,
      user_name,
      home_address1,
      home_address2,
      home_locality,
      home_geo_location,
      home_post_code,
      personal_domain,
      personal_email,
      personal_web,
      personal_phone,
      personal_mobile,
      skin_tone,
      hair_color,
      accessory_color,
  ]

  oap_model = personal_model + [date_of_birth_oap]
  adult_model = personal_model + [date_of_birth_adult]
  parent_model = personal_model + [date_of_birth_parent]
  student_model = personal_model + [date_of_birth_student]
  teen_model = personal_model + [date_of_birth_teen]
  child_model = personal_model + [date_of_birth_child]
  toddler_model = personal_model + [date_of_birth_toddler]
  baby_model = personal_model + [date_of_birth_baby]

  employee_model = adult_model + [employee_number, company_name, department, job_title]

--------------------------------------
imakedata.model.common
--------------------------------------

::

  from imakedata.model.common import common_model

  common_model = [
      pk,
      space,
      nb_space,
      animal_name,
      compound_word,
  ]
