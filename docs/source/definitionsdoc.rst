Definition Guide
==================

Build on the defintion library by writing your own fields and models. Each
column/field in your dataset is defined by a dictionary in a list.

::

  data_def = [
    { define_field1 },
    { define_field2 },
    { define_field3 },
    { define_field4 },
  ]


Each defined field needs a name and a class.

name will the field name used in the results. It should be unique.

::

  animal_name = {
    'name': 'animal_name',
    'class': 'quicklist',
    'data': ['dog', 'cat', 'mouse', 'horse', 'rat']
  },


The class is the programming class where the data is taken from.

The 9 data source classes:

===============
igetraw
===============

A json formatted file containing data (see raw folder). You can add your
own. For those raw files whose data is made of dictionary objects in a list,
it will assign the whole dictionary to the column:


::

  study_area = {
    'name': 'study_area',
    'class': 'igetraw',
    'data': 'academic'
  }


**Result:**

::

  [
    {study_area: {subject: Philosophy, department: Arts}},
    {study_area: {subject: Computing, department: Science}},
    {study_area: {subject: Accounting, department: Business}},
    {study_area: {subject: Physics, department: Science}},
  ]


Options
-------------

property
------------------

Specify one of properties of the dictionary object to return a single column
even if the source contains a dictionary.

::

  {
    'property': 'department',
  }

**Result:**

::

  [
    {study_area: Arts},
    {study_area: Science},
    {study_area: Business},
    {study_area: Science},
  ]

filter
------------------

Filter the results by any property.

::

  {
    'filters': [{department: [Arts, Science]}, ...more filters... ],
  }

**Result:**

::

  [
    {study_area: Arts},
    {study_area: Science},
    {study_area: Arts},
    {study_area: Science},
  ]

===============
quicklist
===============

A user supplied list of choices for each field.

::

  in_the_list = {
    'name': 'in_the_list',
    'class': 'quicklist',
    'data': [1, 'of', 'the', 'items', 'in', 'this', 'list', 4, 'each', 'row']
  }

**Result:**

::

  [
    {in_the_list: of},
    {in_the_list: in},
    {in_the_list: 1},
    {in_the_list: each},
  ]

===============
pk
===============

A unique number for each record.

::

  contact_id = {
    'name': 'contact_id',
    'class': 'pk',
  }


**Result:**

::

  [
    {contact_id: 1},
    {contact_id: 2},
    {contact_id: 3},
    {contact_id: 4},
  ]

===============
primitive
===============

Make columns of numbers, dates, times, decimals, primary keys. min and max
sets a limit and takes any date, integer or decimal, e.g. '1940-01-01', 1, 0.23;
defaults min:1, max:2

::

  product_weight =  {
    'name': 'product_weight',
    'class': 'primitive',
    'method': 'dec_list',
    'min': 1.4,
    'max': 2.8,
  }

**Result:**

::

  [
    {product_weight: 2.4},
    {product_weight: 2.1},
    {product_weight: 1.4},
    {product_weight: 2.0},
  ]


===============
field
===============

The field class clones from data already made in another field. Perfect to use
in concat definitions to make email addresses and URLs which match data other
fields; or when the relationship helps make the data look more realistic.

In the
example below we price the postage cost in line with the product_weight field
above. The field name being referenced must come first in the model definition.

::

  postage_cost = {
    'name': 'postage_cost',
    'class': 'field',
    'data': 'product_weight',
    'calc': [{'multiply': 10}, {'divide': 2}, {'add': 0.99}]
  }


**Result:**

::

  [
    {postage_cost: 12.99},
    {postage_cost: 11.49},
    {postage_cost: 7.99},
    {postage_cost: 10.99},
  ]


===============
iblurb
===============

Make words, titles, sentences, paragraphs, html, etc. method can be any of the
methods in the IMakeBlurb class. min and max represents the number words
which the blurb will contain. language can be any of json file that contains a
word property.  Add your own!

::

  dvd_title = {
    'name': 'dvd_title',
    'class': 'iblurb',
    'method': 'plaintext_title',
    'min': 3,
    'max': 5,
    'language': 'ainu',
  }


**Result:**

::

  [
    {dvd_title: Erum noye upaskuma eani},
    {dvd_title: Orwa toyko pewrekur},
    {dvd_title: Nuye reekoh cipo siyeye seta},
    {dvd_title: Anekuroro yupi keran pirkaike},
  ]


===============
exact
===============

A column of static data. This is very useful as a defintion in the concat
class, for instance using an "@" symbol to build an email address.

::

  tax_code = {
    'name': 'tax_code',
    'class': 'exact',
    'data': 'A',
  }

**Result:**

::

  [
    {tax_code: A},
    {tax_code: A},
    {tax_code: A},
    {tax_code: A},
  ]


===============
concat
===============

Combine a nested list of definitions, and the results from these fields will be
concatonated into the single field. They can defined like any other field, and
can include another concat class; but they should have no name.

::

  what_i_like = {
    'name': 'what_i_like',
    'class': 'concat',
    'data': [
      {
      'class': 'exact',
      'data': 'I like ',
      },
      {
      'class': 'quicklist',
      'data': ['sports', 'music', 'dancing', 'reading'],
      }
     ],
  }

**Result:**

::

  [
    {what_i_like: I like music},
    {what_i_like: I like music},
    {what_i_like: I like reading},
    {what_i_like: I like sports},
  ]


===============
choose
===============

Use this class to choose from a sub list of definitions; and only select one
randomly per record. The sub list can be any of the available definitions nested
as deeply as you like.

::

  color_with_number = {
    'name': 'color_with_number',
    'class': 'choose',
    'data': [
          {
            'class': 'concat',
            'data': [
                      {
                          'class': 'exact',
                          'data': 'Red',
                      },
                      {
                          'class': 'primitive',
                          'method': 'int_list',
                          'min': 2,
                          'max': 5,
                      },
                    ]
          },
          {
            'class': 'concat',
            'data': [
                      {
                          'class': 'exact',
                          'data': 'Green',
                      },
                      {
                          'class': 'primitive',
                          'method': 'int_list',
                          'min': 20,
                          'max': 50,
                      },
                    ]
          },
      ],
  }

**Result:**

::

  [
    {color_with_number: Red2},
    {color_with_number: Green35},
    {color_with_number: Green42},
    {color_with_number: Red4},
  ]


===================
Optional Properties
===================

itransform
------------------

A list of transformation methods from the ITransformData.Field class to format
the data. Multiple transforms can be listed which are processed left->right.

::

  {
    'itransform': [
              'upper',
              'lower',
              'chomp',
              'capitalize',
              'slugify',
              'title',
              ],
  }

calc
------------------

A list of calculations to perform on a field using any methods from the
IAmPrimitiveLists.Calc class. calculations on dates are for hours. Multiple
calculations can be listed and are performed left->right.

::

  {
    'calc': [{'add': 2}, {'subtract': 5}, {'multiply': 10}, {'divide': 3}, ]
  }


splutter
------------------

By default IMakeData fills 100% of fields in a column with data. Use splutter
to make a percentage of fields (on average) blank. i.e. 10 = 10%~ of fields
blank.

::

  {
    'splutter': 10,
  }


remove
------------------

Use this option to remove columns/fields after the data is built - e.g.
if they were temporary for making data available to concat class fields -
e.g. a domain_name used for building email addresses and urls - but not needed
as a column in the results.

::

  {
    'remove': True,
  }

***************
Samples
***************

See the sample folder imakedata.model for ready to use datasets which can
also be used as a base to create your own.
