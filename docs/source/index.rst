.. INeedData documentation master file, created by
   sphinx-quickstart on Fri Mar 24 04:50:12 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


=========================
INeedData documentation
=========================

A source of random but realistic looking data. This app works by combining randomly
generated primitive data and prepared realistic data to create realistic random
records that you can use to test and demo your databases and apps.

The main tool is `imakedata.imakedata.IMakeData`, which is initialised for a certain number
of records, then passed definitions of a data model - which decribes a
instructions for randomly building data into columns.

INeedData provide a set of tools to quickly spin up realistic looking sample
data.



.. toctree::
  :maxdepth: 2
  :caption: Contents:

  core
  modeldoc
  rawdoc
  definitionsdoc


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
