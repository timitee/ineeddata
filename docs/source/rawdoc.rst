RawData
==================

--------------------------------------
imakedata.raw.raw
--------------------------------------

.. automodule:: imakedata.raw.raw
  :members:

--------------------------------------
imakedata.raw.academic
--------------------------------------

.. automodule:: imakedata.raw.academic
  :members:

--------------------------------------
imakedata.raw.ainu
--------------------------------------

.. automodule:: imakedata.raw.ainu
  :members:

--------------------------------------
imakedata.raw.animal_prefix
--------------------------------------

.. automodule:: imakedata.raw.animal_prefix
  :members:

--------------------------------------
imakedata.raw.animal_suffix
--------------------------------------

.. automodule:: imakedata.raw.animal_suffix
  :members:

--------------------------------------
imakedata.raw.animal
--------------------------------------

.. automodule:: imakedata.raw.animal
  :members:

--------------------------------------
imakedata.raw.business_type
--------------------------------------

.. automodule:: imakedata.raw.business_type
  :members:

--------------------------------------
imakedata.raw.car
--------------------------------------

.. automodule:: imakedata.raw.car
  :members:

--------------------------------------
imakedata.raw.char
--------------------------------------

.. automodule:: imakedata.raw.char
  :members:

--------------------------------------
imakedata.raw.danish
--------------------------------------

.. automodule:: imakedata.raw.danish
  :members:

--------------------------------------
imakedata.raw.district_suffix
--------------------------------------

.. automodule:: imakedata.raw.district_suffix
  :members:

--------------------------------------
imakedata.raw.english
--------------------------------------

.. automodule:: imakedata.raw.english
  :members:

--------------------------------------
imakedata.raw.geo_location
--------------------------------------

.. automodule:: imakedata.raw.geo_location
  :members:

--------------------------------------
imakedata.raw.job_title
--------------------------------------

.. automodule:: imakedata.raw.job_title
  :members:

--------------------------------------
imakedata.raw.latin
--------------------------------------

.. automodule:: imakedata.raw.latin
  :members:

--------------------------------------
imakedata.raw.person
--------------------------------------

.. automodule:: imakedata.raw.person
  :members:

--------------------------------------
imakedata.raw.product
--------------------------------------

.. automodule:: imakedata.raw.product
  :members:

--------------------------------------
imakedata.raw.street_type
--------------------------------------

.. automodule:: imakedata.raw.street_type
  :members:

--------------------------------------
imakedata.raw.surname
--------------------------------------

.. automodule:: imakedata.raw.surname
  :members:

--------------------------------------
imakedata.raw.training_course
--------------------------------------

.. automodule:: imakedata.raw.training_course
  :members:

--------------------------------------
imakedata.raw.transport
--------------------------------------

.. automodule:: imakedata.raw.transport
  :members:

--------------------------------------
imakedata.raw.word_prefix
--------------------------------------

.. automodule:: imakedata.raw.word_prefix
  :members:

--------------------------------------
imakedata.raw.word_suffix
--------------------------------------

.. automodule:: imakedata.raw.word_suffix
  :members:
