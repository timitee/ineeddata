Core Classes
==================

======================================
imakedata.imakedata
======================================

.. automodule:: imakedata.imakedata
   :members:

======================================
imakedata.iamprimitive
======================================

.. automodule:: imakedata.iamprimitive
   :members:

======================================
imakedata.igetraw
======================================

.. automodule:: imakedata.igetraw
   :members:

======================================
imakedata.iblurb
======================================

.. automodule:: imakedata.iblurb
   :members:

======================================
imakedata.itransform
======================================

.. automodule:: imakedata.itransform
  :members:

======================================
imakedata.ipictureit
======================================

.. automodule:: imakedata.ipictureit
   :members:
