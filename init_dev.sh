#!/bin/bash
# exit immediately if a command exits with a nonzero exit status.
set -e
# treat unset variables as an error when substituting.
set -u

pytest -x
# touch temp.db && rm temp.db
# django-admin.py migrate --noinput
# django-admin.py 26_animals
# django-admin.py runserver 0.0.0.0:8000
