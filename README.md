![](./timicard.png)

# ineeddata

A source of random but realistic looking data. This app works by combining randomly
generated primitive data and prepared realistic data to create realistic random
records that you can use to test and demo your databases and apps.

The main tool is `IMakeData`, which is initialised for a certain number
of records, then passed definitions of a data model - which decribes a
instructions for randomly building data into columns.

INeedData provide a set of tools to quickly spin up realistic looking sample
data.


## Virtual Environment


```
virtualenv --python=python3.5 venv-ineeddata
source venv-ineeddata/bin/activate
export PS1='(venv_ind):$ '

pip install -r requirements/local.txt
```

## Testing

```
find . -name '*.pyc' -delete
py.test -x
```

## Usage

```
from imakedata.imakedata import IMakeData
number_records = 10
maker = IMakeData(number_records)
```

### Build from ready made field-definitions:

```
from imakedata.ijusthelp import rewrite_dict
from imakedata.model.marketing import product_title, postage_cost
from imakedata.model.date import within_days

delivery_date = rewrite_dict(within_days, {'name': 'delivery_date'})

my_data_set = maker.get_data([product_title, postage_cost, delivery_date])
print(my_data_set)
```

### Build from large, but ready-made model definitions:

```
from imakedata.model.personal import employee_model
my_data_set = maker.get_data(employee_model)
print(my_data_set)
```

## Samples

See the model folder for ready to use column definitions and models which can
also be used as a base to create your own.


![](./appicon.png)