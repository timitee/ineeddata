# -*- encoding: utf-8 -*-
"""Ready made column definitions for time data."""

from imakedata.ijusthelp import rewrite_dict
from imakedata.model.primitive import time_list

from datetime import timedelta
from datetime import datetime
mn = timedelta(minutes=1)
hr = timedelta(hours=1)
n = datetime.now()

time_short_format = '{:%H:%M:%S}'
time_long_format = '{:%H:%M:%S.%f}'

last_minute = rewrite_dict(time_list, {
    'name': 'last_minute',
    'min': time_long_format.format(n - mn),
    'max': time_long_format.format(n),
})

last_hour = rewrite_dict(time_list, {
    'name': 'last_hour',
    'min': time_long_format.format(n - hr),
    'max': time_long_format.format(n),
})

within_minutes = rewrite_dict(time_list, {
    'name': 'within_minutes',
    'min': time_short_format.format(n),
    'max': time_short_format.format(n + (mn * 30)),
})

within_hours = rewrite_dict(time_list, {
    'name': 'within_hours',
    'min': time_short_format.format(n),
    'max': time_short_format.format(n + (hr * 4)),
})

opening_hours = rewrite_dict(time_list, {
    'name': 'opening_hours',
    'min': time_short_format.format(datetime(n.year, n.month, n.day, 8, 0, 0)),
    'max': time_short_format.format(datetime(n.year, n.month, n.day, 18, 30, 0)),
})

last_24 = rewrite_dict(time_list, {
    'name': 'last_24',
    'min': time_short_format.format(n - (hr * 24)),
    'max': time_short_format.format(n),
})

time_model = [
    last_minute,
    last_hour,
    within_minutes,
    within_hours,
    opening_hours,
    last_24,
]
