# -*- encoding: utf-8 -*-
"""
A base for raw data.

**Usage:**

::

    from imakedata.imakedata import IMakeData

    raw = {
        'name': 'raw',
        'class': 'igetraw',
        'data': 'raw',
    }

    from imakedata.raw.raw import raw
    number_records = 10
    maker = IMakeData(number_records)

    data_set = maker.get_data([raw])

    for row in data_set:
        ineeddata.raw = row['raw']



From my answer to a Quora question
https://www.quora.com/What-is-your-original-26-word-story-where-each-word-begins-with-a-different-letter-of-the-English-alphabet/answer/Tim-Bushell

"""



raw = [

    'A',

        'brave,',

            'chance',

                'dance',

                    'ended',



                     'Fred\'s', 'girlfriend', 'hunt',



                'Ingrid', 'just', 'kissed', 'like', 'magic!',



        'once', 'privately', 'quartered,',

    'Nearby,',           'romance', 'secured', 'the', 'unfolding', 'victory',

                                                                    'with',

                                                                    'X-rated,',

                                                                    'youthful',

                                                                    'zest.',
]
