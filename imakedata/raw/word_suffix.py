# -*- encoding: utf-8 -*-
"""
A list of 150 or so words that you'll typically find at the end of English
words, like ous, able, ster, ize, etc. You can use them to add a greater
degree of randomness and flexibility when creating data with any of the
dictionaries in this program.

**Usage:**

::

    from imakedata.imakedata import IMakeData
    from imakedata.ijusthelp import rewrite_dict
    from imakedata.model.raw import raw

    word_suffix_raw = rewrite_dict(raw, {
        'name': 'word_suffix_raw',
        'data': 'word_suffix',
    })

    from imakedata.model.raw import word_suffix_raw
    number_records = 10
    maker = IMakeData(number_records)

    data_set = maker.get_data([word_suffix_raw])

    for row in data_set:
        ineeddata.word_suffix = row['word_suffix_raw']

"""

word_suffix = [
    'gerous', 'ferous', 'hood', 'dores', 'zuge', 'lin', 'cropped', 'ancy',
    'fishes', 'nies', 'taroth', 'ical', 'naires', 'pos', 'type', 'olling', 'si',
    'ic', 'drosten', 'raking', 'arch', 'one', 'tre', 'strucken', 'cae', 'ances',
    'mere', 'rias', 'lae', 'gies', 'shioth', 'or', 'handled', 'da', 'trixes', 'nos',
    'sporangia', 'ation', 'ghi', 'ship', 'izing', 'ency', 'ible', 'omata', 'taks',
    'vetted', 'fish', 'oled', 'ated', 'bies', 'raaden', 'able', 'ness', 'blast',
    'cropping', 'ol', 'lions', 'ling', 'ces', 'gram', 'wards', 'staffs', 'some',
    'nea', 'deaux', 'ous', 'neums', 'bunde', 'la', 'ist', 'vetting', 'berries',
    'trices', 'dromed', 'scope', 'nomata', 'ric', 'ism', 'oma', 'itis', 'nieres',
    'ries', 'ties', 'ward', 'plastic', 'fy', 'logy', 'ment', 'staves', 'nized',
    'li', 'thropi', 'uret', 'genous', 'typing', 'mi', 'derm', 'ias', 'ancies',
    'schaften', 'in', 'er', 'ing', 'lite', 'en', 's', 'ana', 'anesthesia', 'dom',
    'plasty', 'triller', 'nemia', 'tos', 'ite', 'men', 'lith', 'tries', 'handling',
    'vated', 'est', 'poda', 'cating', 'soria', 'las', 'tyla', 'prossing', 'goes',
    'ons', 'morphous', 'nizing', 'rid', 'kies', 'oling', 'tin', 'docenten', 'ize',
    'ating', 'let', 'droming', 'vating', 'ory', 'cated', 'ess', 'grave', 'meter',
    'nues', 'yos', 'gos', 'toxemia', 'ance', 'ably', 'ti', 'lea', 'head', 'ion',
    'mo', 'olled', 'ise', 'ni', 'rae', 'mancy', 'ga', 'ses', 'gen', 'ria', 'esque',
    'retted', 'graphy', 'metry', 'ock', 'ful', 'ies', 'ee', 'ics', 'pod', 'lumus',
    'ent', 'ways', 'et', 'seuses', 'barrelled', 'ine', 'art', 'sane', 'ide', 'ose',
    'less', 'tiches', 'ums', 'min', 'prossed', 'ized', 'sories', 'shoth', 'raked',
    'mieres', 'ly', 'chore', 'ence', 'ive', 'ard', 'ate', 'schiebungen', 'our',
    'esprits', 'cies', 'typed', 'escent', 'women', 'seurs', 'ed', 'knives', 'emia',
    'ster', 'ish', 'mata', 'cales', 'zin', 'oid', 'yl', 'ant', 'diecious',
]
