# -*- encoding: utf-8 -*-
"""
A single character.

**Usage:**

::

    from imakedata.imakedata import IMakeData
    from imakedata.ijusthelp import rewrite_dict
    from imakedata.model.raw import raw

    char_raw = rewrite_dict(raw, {
        'name': 'char_raw',
        'data': 'char',
    })

    from imakedata.raw.raw import char_raw
    number_records = 10
    maker = IMakeData(number_records)

    data_set = maker.get_data([char_raw])

    for row in data_set:
        ineeddata.raw.ascii = row['char_raw']['ascii']
        ineeddata.raw.lower = row['char_raw']['lower']
        ineeddata.raw.upper = row['char_raw']['upper']
        ineeddata.raw.digits = row['char_raw']['digit']
        ineeddata.raw.hexdigits = row['char_raw']['hexdigit']
        ineeddata.raw.octdigits = row['char_raw']['octdigit']
        ineeddata.raw.printable = row['char_raw']['printable']
        ineeddata.raw.punctuation = row['char_raw']['punctuation']
        ineeddata.raw.whitespace = row['char_raw']['whitespace']


The char data is compiled from these raw data sets:

::

    ascii_letter_raw = rewrite_dict(raw, {'name': 'char_raw', 'data': 'ascii_letter'})
    ascii_lowercase_raw = rewrite_dict(raw, {'name': 'ascii_lowercase_raw', 'data': 'ascii_lowercase'})
    ascii_uppercase_raw = rewrite_dict(raw, {'name': 'ascii_uppercase_raw', 'data': 'ascii_uppercase'})
    digit_raw = rewrite_dict(raw, {'name': 'digit_raw', 'data': 'digit'})
    hexdigit_raw = rewrite_dict(raw, {'name': 'hexdigit_raw', 'data': 'hexdigit'})
    octdigit_raw = rewrite_dict(raw, {'name': 'octdigit_raw', 'data': 'octdigit'})
    printable_raw = rewrite_dict(raw, {'name': 'printable_raw', 'data': 'printable'})
    punctuation_raw = rewrite_dict(raw, {'name': 'punctuation_raw', 'data': 'punctuation'})
    whitespace_raw = rewrite_dict(raw, {'name': 'whitespace_raw', 'data': 'whitespace'})

"""
import string

ascii_letter = list(string.ascii_letters)
ascii_lowercase = list(string.ascii_lowercase)
ascii_uppercase = list(string.ascii_uppercase)
digit = list(string.digits)
hexdigit = list(string.hexdigits)
octdigit = list(string.octdigits)
printable = list(string.printable)
punctuation = list(string.punctuation)
whitespace = list(string.whitespace)

char = (
    [{'classification':'ascii', 'char': r} for r in ascii_letter] +
    [{'classification':'lower', 'char': r} for r in ascii_lowercase] +
    [{'classification':'upper', 'char': r} for r in ascii_uppercase] +
    [{'classification':'digit', 'char': r} for r in digit] +
    [{'classification':'hexdigit', 'char': r} for r in hexdigit] +
    [{'classification':'octdigit', 'char': r} for r in octdigit] +
    [{'classification':'printable', 'char': r} for r in printable] +
    [{'classification':'punctuation', 'char': r} for r in punctuation] +
    [{'classification':'whitespace', 'char': r} for r in whitespace]
)
