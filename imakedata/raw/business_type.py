# -*- encoding: utf-8 -*-
"""
Business Name suffixes.

**Usage:**

::

    from imakedata.imakedata import IMakeData
    from imakedata.ijusthelp import rewrite_dict
    from imakedata.model.raw import raw

    business_type_raw = rewrite_dict(raw, {
        'name': 'business_type_raw',
        'data': 'business_type',
    })

    from imakedata.model.raw import business_type_raw
    number_records = 10
    maker = IMakeData(number_records)

    data_set = maker.get_data([business_type_raw])

    for row in data_set:
        ineeddata.business_type = row['business_type_raw']

"""

business_type = [
    'Ltd', 'Plc', '& Co', '& Partners', 'Inc', '& Son', '& Sons', 'Associated',
    'Corporation', 'Incorporated', 'International',
]
