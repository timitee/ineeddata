# -*- encoding: utf-8 -*-
"""
A list of training courses with categories.

**Usage:**

::

    from imakedata.imakedata import IMakeData
    from imakedata.ijusthelp import rewrite_dict
    from imakedata.model.raw import raw

    training_course_raw = rewrite_dict(raw, {
        'name': 'training_course_raw',
        'data': 'training_course',
    })

    from imakedata.model.raw import training_course_raw
    number_records = 10
    maker = IMakeData(number_records)

    data_set = maker.get_data([training_course_raw])

    for row in data_set:
        ineeddata.subject = row['training_course_raw']


"""

training_course = ['TODO']
