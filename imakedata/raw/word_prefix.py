# -*- encoding: utf-8 -*-
"""
A list of 150 or so words that you'll typically find at the front of English
words, like pre, en, in, sub, etc. You can use them to add a greater degree
of randomness and flexibility when creating data with any of the dictionaries
in this program.

**Usage:**

::

    from imakedata.imakedata import IMakeData
    from imakedata.ijusthelp import rewrite_dict
    from imakedata.model.raw import raw

    word_prefix_raw = rewrite_dict(raw, {
        'name': 'word_prefix_raw',
        'data': 'word_prefix',
    })

    from imakedata.model.raw import word_prefix_raw
    number_records = 10
    maker = IMakeData(number_records)

    data_set = maker.get_data([word_prefix_raw])

    for row in data_set:
        ineeddata.subject = row['word_prefix_raw']['subject']
        ineeddata.department = row['word_prefix_raw']['department']


Make a compound word.

::

    from imakedata.imakedata import IMakeData
    from imakedata.model.raw import word_prefix_raw, english_word_raw, word_suffix_raw

    compound_word =  {
        'name': 'compound_word',
        'class': 'concat',
        'data': [word_prefix_raw, english_word_raw, word_suffix_raw],
    }

    from imakedata.model.common import compound_word
    number_records = 10
    maker = IMakeData(number_records)


    data_set = maker.get_data([compound_word])

    for row in data_set:
        ineeddata.compound_word = row['compound_word']

"""

word_prefix = [
    'ad', 'aden', 'adeno', 'aero', 'al', 'amb', 'ambi', 'amphi', 'ana', 'angio',
    'anglo', 'ant', 'ante', 'antero', 'arch', 'archi', 'astro', 'auto', 'azo',
    'basi', 'be', 'bi', 'bin', 'bis', 'chloro', 'chondro', 'circum', 'cis', 'co',
    'col', 'com', 'con', 'conico', 'cor', 'countre', 'cyclo', 'de', 'deca', 'deka',
    'demi', 'deut', 'deuto', 'dextro', 'di', 'dia', 'diamido', 'diazo', 'dis',
    'dys', 'e', 'ect', 'ecto', 'electro', 'em', 'en', 'end', 'endo', 'ent', 'enter',
    'ento', 'ep', 'epi', 'equi', 'ex', 'extra', 'ferri', 'ferro', 'ferroso', 'fluo',
    'for', 'fronto', 'gastro', 'ge', 'gutturo', 'haema', 'haemato', 'haemo',
    'helio', 'hema', 'hemi', 'hemo', 'hetero', 'hex', 'holo', 'homo', 'hydr',
    'hydro', 'hydroxy', 'hyo', 'hyper', 'hypo', 'i', 'ideo', 'idio', 'il', 'ilio',
    'im', 'in', 'indo', 'inter', 'intra', 'intro', 'iod', 'iodo', 'ir', 'is', 'iso',
    'isonitroso', 'kilo', 'laevo', 'leuc', 'leuco', 'levo', 'luteo', 'macro',
    'magneto', 'mal', 'male', 'meg', 'megalo', 'mes', 'meso', 'met', 'meta', 'micr',
    'micro', 'milli', 'mis', 'mon', 'mono', 'mult', 'multi', 'myo', 'myria', 'naso',
    'nemato', 'neo', 'neuro', 'nitro', 'nitroso', 'non', 'ob', 'occipito', 'octa',
    'octo', 'oculo', 'odonto', 'oligo', 'omni', 'omo', 'omphalo', 'organo',
    'ornitho', 'ortho', 'osteo', 'oto', 'oxy', 'pachy', 'palaeo', 'palato', 'paleo',
    'pan', 'panta', 'panto', 'para', 'pari', 'parieto', 'pedi', 'pedo', 'penta',
    'per', 'peri', 'petro', 'philo', 'phono', 'photo', 'phyllo', 'physico', 'phyto',
    'plani', 'plano', 'platy', 'pleuro', 'pluri', 'pneumato', 'pneumo', 'podo',
    'poly', 'post', 'prae', 'praeter', 'praseo', 'pre', 'preter', 'pro', 'proto',
    'pseudo', 'psycho', 'purpureo', 'pyr', 'pyro', 'quadri', 'quinque', 'ra',
    'radio', 're', 'recti', 'recto', 'retro', 'rhino', 'roseo', 'sacro', 'sarco',
    'scapulo', 'schizo', 'selenio', 'semi', 'septi', 'sesqui', 'sex', 'silico',
    'sodio', 'spermato', 'spermo', 'spheno', 'stanno', 'stannoso', 'step', 'stereo',
    'sterno', 'stylo', 'sub', 'subsesqui', 'suf', 'sulphato', 'sulpho', 'super',
    'sur', 'sym', 'syn', 'tarso', 'tartro', 'temporo', 'ter', 'tetra', 'tetrazo',
    'thermo', 'thio', 'thyro', 'tibio', 'titano', 'to', 'trans', 'tri', 'tympano',
    'ultra', 'un', 'uni', 'uranoso', 'uro', 'valero', 'ventro', 'vertebro',
    'vesico', 'xantho', 'xylo', 'y', 'zinco', 'zirco', 'zoo',
]
