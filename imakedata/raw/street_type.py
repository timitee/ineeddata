# -*- encoding: utf-8 -*-
"""
Words like Street, Road, Lane, etc, which can be neatly appended to words and
phrases to create realistic address data.

**Usage:**

::

    from imakedata.imakedata import IMakeData
    from imakedata.ijusthelp import rewrite_dict
    from imakedata.model.raw import raw

    street_type_raw = rewrite_dict(raw, {
        'name': 'street_type_raw',
        'data': 'street_type',
    })

    from imakedata.model.raw import street_type_raw
    number_records = 10
    maker = IMakeData(number_records)

    data_set = maker.get_data([street_type_raw])

    for row in data_set:
        ineeddata.subject = row['street_type_raw']['subject']
        ineeddata.department = row['street_type_raw']['department']


Create a definition which concatonates a latin word to a street_type to
make a road name

::

    from imakedata.imakedata import IMakeData
    from imakedata.model.common import nb_space
    from imakedata.model.int import in_100
    from imakedata.model.raw import animal_name_raw, street_type_raw

    address1 =   {
        'name': 'address1',
        'class': 'concat',
        'data': [
                in_100,
                nb_space,
                animal_name_raw,
                nb_space,
                street_type_raw,
            ]
    }

    from imakedata.model.location import address1
    number_records = 10
    maker = IMakeData(number_records)

    data_set = maker.get_data([address1])

    for row in data_set:
        ineeddata.address.locality = row['address1']

"""

street_type = [
    'Road', 'Street', 'Way', 'Lane', 'Avenue', 'Green', 'Close', 'Boulevard',
    'Terrace', 'Hill', 'Pen', 'Park Avenue', 'Park Road', 'Heights', 'Road',
    'Street', 'Way', 'Lane', 'Avenue', 'Green', 'Close',
]
