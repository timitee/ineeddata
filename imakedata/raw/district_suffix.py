# -*- encoding: utf-8 -*-
"""
A list of words commonly used to suffix urban and country locations.

**Usage:**

::

    from imakedata.imakedata import IMakeData
    from imakedata.ijusthelp import rewrite_dict
    from imakedata.model.raw import raw

    district_suffix_raw = rewrite_dict(raw, {
        'name': 'district_suffix_raw',
        'data': 'district_suffix',
    })

    from imakedata.model.raw import district_suffix_raw
    number_records = 10
    maker = IMakeData(number_records)

    data_set = maker.get_data([district_suffix_raw])

    for row in data_set:
        ineeddata.district_suffix = row['district_suffix_raw']


Create a definition which concatonates a latin word to a district_suffix

::

    from imakedata.imakedata import IMakeData
    from imakedata.model.raw import latin_raw, district_suffix_raw

    locality = {
        'name': 'locality_field',
        'class': 'concat',
        'itransform': ['capitalize'],
        'data': [latin_raw, district_suffix_raw],
    }

    from imakedata.model.location import locality

    number_records = 10
    maker = IMakeData(number_records)

    data_set = maker.get_data([locality])

    for row in data_set:
        ineeddata.address.locality = row['locality']

"""

district_suffix = [
    'dale',
    'field',
    'ham',
    'water',
    'wood',
    ' Bridge',
    ' by the River',
    ' City',
    ' Climbs',
    ' Cold',
    ' Common',
    ' Dales',
    ' Dell',
    ' District',
    ' East',
    ' Field',
    ' Flats',
    ' Farm',
    ' Knowles',
    ' Farms',
    ' Heights',
    ' Hills',
    'dale',
    'field',
    'ham',
    'water',
    'wood',
    ' in the Hole',
    ' in the Wood',
    ' Knarl',
    ' Knot',
    ' Mole',
    ' North',
    ' North East',
    ' North West',
    'dale',
    'field',
    'ham',
    'water',
    'wood',
    ' off the Track',
    ' on the Dell',
    ' on the Hill',
    ' Park',
    ' Plains',
    ' Rains',
    ' River',
    ' South',
    ' South East',
    ' South West',
    ' Springs',
    ' Valley',
    ' Waters',
    ' West',
    ' Woods',
    'dale',
    'field',
    'ham',
    'water',
    'wood',
]
