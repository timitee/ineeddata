# -*- encoding: utf-8 -*-
import pytest
from imakedata.ijusthelp import rewrite_dict, get_single_from_list
from imakedata.imakedata import IMakeData

from imakedata.model.raw import raw

class TestClass:

    def test_record(self):
        number_records = 1
        maker = IMakeData(number_records)
        d = maker.get_data([raw])
        assert len(d) == number_records

    def test_records(self):
        number_records = 10000
        maker = IMakeData(number_records)
        d = maker.get_data([raw])
        assert len(d) == number_records

    def test_get_single(self):
        number_records = 10000
        maker = IMakeData(number_records)
        d = maker.get_data([raw])
        item = get_single_from_list(d)
        assert isinstance(item['raw'], str)
