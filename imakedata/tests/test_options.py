# -*- encoding: utf-8 -*-
import pytest
from slugify import slugify
from imakedata.ijusthelp import rewrite_dict
from imakedata.imakedata import IMakeData

from imakedata.model.raw import ascii_lowercase_raw, ascii_uppercase_raw, academic_raw
from imakedata.raw.raw import raw

complex_string = ' '.join(raw)

transform_tester = {
    'name': 'transform_tester',
    'class': 'quicklist',
    'data': [complex_string]
}


class TestClass:

    large_number_records = 10000
    transform_maker = IMakeData(large_number_records)

    def test_transform_splutter(self):
        percent = 50
        target = self.large_number_records * (percent / 100)
        leeway = self.large_number_records * (10 / 100) #%
        d = self.transform_maker.get_data([
                    rewrite_dict(transform_tester, {'splutter': percent})
                    ])
        d_not_blank = [item for item in d if item['transform_tester']]
        assert len(d_not_blank) > (target - leeway) and len(d_not_blank) < (target + leeway)


    def test_transform_filter(self):
        no_dept = 'Business'
        d = self.transform_maker.get_data([
                        academic_raw
                    ])
        assert no_dept in [item['academic_raw']['department'] for item in d]

        d = self.transform_maker.get_data([
                    rewrite_dict(academic_raw, {
                        'filters': [{ 'department': ['Arts', 'Science']},]
                        })
                    ])
        assert no_dept not in [item['academic_raw']['department'] for item in d]

    def test_transform_remove(self):
        resp = 'NOSUCHCOLUMN!'
        d = self.transform_maker.get_data([
                    ascii_lowercase_raw,
                    rewrite_dict(ascii_uppercase_raw, {'remove': True,})
                    ])
        assert d[0].get('ascii_uppercase_raw', resp) == resp
