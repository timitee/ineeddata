# -*- encoding: utf-8 -*-
import pytest
from imakedata.ijusthelp import rewrite_dict
from imakedata.imakedata import IMakeData

from imakedata.model.int import one_or_two


class TestClass:
    num = 5
    calc_maker = IMakeData(1)
    test_calc = rewrite_dict(one_or_two, {
        'name': 'test_calc',
        'min': num,
        'max': num,
        'calc': [{'add': 2}, {'subtract': 5}, {'multiply': 10}, {'divide': 3},]
    })

    def test_calc_add(self):
        d = self.calc_maker.get_data([
                    rewrite_dict(self.test_calc, {'calc': [{'add': self.num}],})
                    ])
        assert d[0]['test_calc'] == self.num + self.num

    def test_calc_subtract(self):
        d = self.calc_maker.get_data([
                    rewrite_dict(self.test_calc, {'calc': [{'subtract': self.num}],})
                    ])
        assert d[0]['test_calc'] == self.num - self.num


    def test_calc_multiply(self):
        d = self.calc_maker.get_data([
                    rewrite_dict(self.test_calc, {'calc': [{'multiply': self.num}],})
                    ])
        assert d[0]['test_calc'] == self.num * self.num


    def test_calc_divide(self):
        d = self.calc_maker.get_data([
                    rewrite_dict(self.test_calc, {'calc': [{'divide': self.num}],})
                    ])
        assert d[0]['test_calc'] == self.num / self.num


    def test_calc_combine(self):
        d = self.calc_maker.get_data([
                    rewrite_dict(self.test_calc, {'calc': [
                                                    {'multiply': 10},
                                                    {'add': 2},
                                                    {'subtract': 5},
                                                    {'divide': 3},
                                                ],})
                    ])
        assert d[0]['test_calc'] == ((((self.num * 10) + 2) - 5) / 3)
