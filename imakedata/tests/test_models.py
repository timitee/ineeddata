# -*- encoding: utf-8 -*-
import pytest
from imakedata.model.raw import raw_model
from imakedata.model.blurb import blurb_model
from imakedata.model.business import business_model
from imakedata.model.common import common_model
from imakedata.model.date import date_model
from imakedata.model.float import float_model
from imakedata.model.int import int_model
from imakedata.model.location import location_model
from imakedata.model.marketing import marketing_model
from imakedata.model.personal import personal_model
from imakedata.model.primitive import primitive_model
from imakedata.model.time import time_model

from imakedata.ijusthelp import rewrite_dict
from imakedata.imakedata import IMakeData
number_records = 3
maker = IMakeData(number_records)


class TestClass:

    def test_raw(self):
        d = maker.get_data(raw_model)
        assert len(d) == number_records

    def test_blurb(self):
        d = maker.get_data(blurb_model)
        assert len(d) == number_records

    def test_business(self):
        d = maker.get_data(business_model)
        assert len(d) == number_records

    def test_common(self):
        d = maker.get_data(common_model)
        assert len(d) == number_records

    def test_date(self):
        d = maker.get_data(date_model)
        assert len(d) == number_records

    def test_float(self):
        d = maker.get_data(float_model)
        assert len(d) == number_records

    def test_int(self):
        d = maker.get_data(int_model)
        assert len(d) == number_records

    def test_location(self):
        d = maker.get_data(location_model)
        assert len(d) == number_records

    def test_marketing(self):
        d = maker.get_data(marketing_model)
        assert len(d) == number_records

    def test_personal(self):
        d = maker.get_data(personal_model)
        assert len(d) == number_records

    def test_primitive(self):
        d = maker.get_data(primitive_model)
        assert len(d) == number_records

    def test_time(self):
        d = maker.get_data(time_model)
        assert len(d) == number_records
