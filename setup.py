import os
from distutils.core import setup


def read_file_into_string(filename):
    path = os.path.abspath(os.path.dirname(__file__))
    filepath = os.path.join(path, filename)
    try:
        return open(filepath).read()
    except IOError:
        return ''


def get_readme():
    for name in ('README', 'README.rst', 'README.md'):
        if os.path.exists(name):
            return read_file_into_string(name)
    return ''


setup(
    name='ineeddata',
    packages=['imakedata', 'imakedata.model', 'imakedata.raw'],
    package_data={
        'imakedata': [
            'faces/*.*',
            'images/animal/*.*',
            'images/business/*.*',
            'images/flag/*.*',
            'images/people/*.*',
        ],
    },
    version='0.0.8',
    description='INeedData',
    author='Tim Bushell',
    author_email='tcbushell@gmail.com',
    url='git@github.com:timitee/ineeddata.git',
    classifiers=[
        'Development Status :: 1 - Planning',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Framework :: Django :: 1.10',
        'Topic :: Development :: API',
    ],
    long_description=get_readme(),
)
